// import CrudApp from './components/CrudApp'; // Crud estatico 
// import CrudApi from './components/CrudApi'; // Crud Con Json Server
// import SongSearch from './components/SongSearch'; // Buscador de canciones a una Api externa
import SelectAnidados from './components/SelectAnidados';
import './styles/estilos.scss';

function App() {

  let Styles = {
    padding:"1rem",
    margin:"1rem"
}
  return (
    <div style={Styles} className="App">
      <SelectAnidados/>        
    </div>
  );
}

export default App;
