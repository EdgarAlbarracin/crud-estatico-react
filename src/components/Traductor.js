import React, {Component} from "react";

export default class TraductorMateo extends Component{

        state = {
          title: "Hola Mundo",
          name :"mateo",
          apellido : "Ceballos",
        };

      updateText =(e)=> {
        if(this.state.title === "Hola Mundo"){
            this.setState({
                title: "Hello World",
                name : "Edgar"
              });
        }else{
            this.setState({
                title: "Hola Mundo",
                name : "Mateo"
              });
        }
      }

    render() {
        return <h1 onClick={this.updateText}>{this.state.title} mi nombre es {this.state.name}</h1>;
      }
}