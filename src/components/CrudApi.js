
import React, { useState,useEffect } from 'react';

import CrudForm from "./CrudForm";
import CrudTable from './CrudTable';  
import { helpHttp } from '../helpers/helpHttp';
import Message from './Message';
import Loader from './Loader';

    
    const CrudApi = () => {

        const [db, setDb] = useState(null)

        const [dataToEdit, setDataToEdit] = useState(null)

        const [error, setError] = useState(null)

        const [loading, setLoading] = useState(false)

        let url = "http://localhost:5000/Local";

        useEffect(() => {

            setLoading(true)
            helpHttp().get(url).then((resp)=>{
                console.log(resp)
                if(!resp.err){
                    setDb(resp)
                    setError(false)
                }else{
                    setDb([])
                    setError(resp)
                }                
            })
            setLoading(false)
        }, [url])


        const createData = (data) => {
            data.id = Date.now()
            console.log('creando', data)
            let options = {body:data, headers:{"content-type":"application/json"}};
            setLoading(true)
            helpHttp().post(url,options).then((resp)=>{
                console.log(resp)
                if(!resp.err){
                    setDb([...db,data])
                    setError(false)
                    alert("Jugador creado con exito")
                    
                }else{
                    setError(resp)
                }                
            })
            setLoading(false)
            
        }

        const updateData = (data) => {

            let urlUp = `${url}/${data.id}`
            let upData = db.map(element => (element.id === data.id ? data : element))
            let options = {body:data, headers:{"content-type":"application/json"}};
            setLoading(true)
            helpHttp().put(urlUp,options).then((resp)=>{
                console.log(resp)
                if(!resp.err){
                    setDb(upData)
                    setError(false)
                    alert("Jugador actualizado con exito")
                    
                }else{
                    setError(resp)
                }                
            })
            setLoading(false)
        }

        const deleteData = (id) => {

            console.log("initalDb", db)
            console.log("id", id)

            let urlDel = `${url}/${id}`
            let options = {headers:{"content-type":"application/json"}}

            setLoading(true)
            helpHttp().put(urlDel,options).then((resp)=>{
                console.log(resp)
                if(!resp.err){
                    
                    let newDta = db.filter(element => element.id !== id)
                    setDb(newDta);
                    setError(false)
                    alert("Eliminación correcta")
                    
                }else{
                    setError(resp)
                    alert("No se pudo encontrar este registro")
                }                
            })
            setLoading(false)


            let indexReg = db.findIndex(element => element.id == id);
            
            if(indexReg>=0){
                

                
            }else{
                
            }
        }

        return (
            <div>
                <h2>CRUD CON JSON SERVER REACT</h2>
                <CrudForm initalDb={db} createData={createData} updateData={updateData} setDataToEdit={setDataToEdit} dataToEdit={dataToEdit} />
                {loading && <Loader/>}
                {error && <Message Msg={`Error ${error.status}: ${error.statusText}`} bgColor="#dc3545" />}
                {db && <CrudTable data={db} setDataToEdit={setDataToEdit} deleteData={deleteData} />}
            </div>
        )
    }

    export default CrudApi
