const Message = ({Msg,bgColor}) => {

    let Styles = {
        padding:"1rem",
        margin:"1rem",
        borderRadius:"1rem",
        marginBottom:"1rem",
        textAlign:"center",
        color: "#fff",
        fontWeight: "Bold",
        backgroundColor :bgColor
    }
    return (
        <div style={Styles}>
            <p>{Msg}</p>
        </div>
    )
}

export default Message
