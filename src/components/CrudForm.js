import React, { useState, useEffect } from 'react';
import { InputGroup, FormControl, Button } from 'react-bootstrap';
import { GiTShirt } from 'react-icons/gi';
import { FaRegIdCard } from 'react-icons/fa';
import { AiOutlineUnorderedList,AiOutlineSend } from 'react-icons/ai';
import { CgDanger } from 'react-icons/cg';


const initialForm = {
    id : null,
    Dorsal :"",
    Nombre :"",
    Posicion :"",
}

const CrudForm = (props) => {

    const [form, setForm] = useState(initialForm)

    useEffect(() => {
        if(props.dataToEdit){
            setForm(props.dataToEdit)
        }
        else{
            setForm(initialForm)
        }
    }, [props.dataToEdit])

    const handleChange = (e) => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = (e) => {
        e.preventDefault()

        if(!form.Dorsal || !form.Nombre || !form.Posicion){
            alert("Datos incompletos")
            return
        }
        // eslint-disable-next-line eqeqeq
        let ExistsDorsal = (form.id!=null ? props.initalDb.filter(element => (element.Dorsal == form.Dorsal && element.id != form.id)):props.initalDb.filter(element => element.Dorsal == form.Dorsal))

        if(ExistsDorsal.length > 0){
            alert(`El número de Dorsal ${ExistsDorsal[0].Dorsal} ya lo tiene el jugador ${ExistsDorsal[0].Nombre}`)
            return
            }

        if(form.id!=null){
            props.updateData(form)
        }else{
            props.createData(form)
        }        
       
        handleReset()
    }
    
    const handleReset = (e) => {
        setForm(initialForm)
        props.setDataToEdit(null)
    }

    return (
        <div>
            <h3>{props.dataToEdit ? "Editar Jugador":"¡Agrega un jugador a la plantilla!"}</h3>
            <form onSubmit={handleSubmit}>
            <InputGroup className="mb-3">
                <InputGroup.Text id="basic-addon1"><GiTShirt/></InputGroup.Text>
                <FormControl type="number" name="Dorsal" onChange={handleChange} value={form.Dorsal} placeholder="Dorsal" aria-label="Dorsal"/>
            </InputGroup>

            <InputGroup className="mb-3">
                <InputGroup.Text id="basic-addon1"><FaRegIdCard/></InputGroup.Text>
                <FormControl type="text" name="Nombre" placeholder="Nombre" onChange={handleChange} value={form.Nombre}/>
            </InputGroup>

            <InputGroup className="mb-3">
                <InputGroup.Text id="basic-addon1"><AiOutlineUnorderedList/></InputGroup.Text>
                <FormControl type="text" name="Posicion" placeholder="Posición" onChange={handleChange} value={form.Posicion} />
            </InputGroup>
            
            <div className="container-buttons">
                <Button variant="danger" type="reset" value="Limpiar" onClick={handleReset}><CgDanger/> Eliminar</Button>
                <Button variant="success" type="submit" value="Enviar"><AiOutlineSend/> Enviar</Button>
            </div>
            </form>
        </div>
    )
}

export default CrudForm
