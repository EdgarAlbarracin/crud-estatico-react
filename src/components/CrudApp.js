
    import React, { useState } from 'react';

    import CrudForm from "./CrudForm";
    import CrudTable from './CrudTable';  

    const initalDb = [
            {
                
                id : 1,
                Dorsal: 7,
                Nombre: "Cristiano Ronaldo",
                Posicion : "Delantero"
            },
            {
                id : 2,
                Dorsal: 11,
                Nombre: "Sancho",
                Posicion : "Lateral Derecho"
            },
            {
                id : 3,
                Dorsal: 9,
                Nombre: "Rashford",
                Posicion : "Lateral Izquierdo"
            },
            {
                id : 4,
                Dorsal: 10,
                Nombre: "Messi",
                Posicion : "Mediocampista ofensivo"
            },
            {
                id : 5,
                Dorsal: 12,
                Nombre: "Kimmich",
                Posicion : "Mediocampista defensivo"
            },
            {
                id : 6,
                Dorsal: 6,
                Nombre: "Guardiola",
                Posicion : "Mediocampista"
            },
            {
                id : 7,
                Dorsal: 17,
                Nombre: "Alphonso Davies",
                Posicion : "Volante Izquierdo"
            },
            {
                id : 8,
                Dorsal: 4,
                Nombre: "Virgil van Dijk",
                Posicion : "Central"
            },
            {
                id : 9,
                Dorsal: 3,
                Nombre: "Kimpembe",
                Posicion : "Central"
            },
            {
                id : 10,
                Dorsal: 66,
                Nombre: "Alexander-Arnold",
                Posicion : "Volante Derecho"
            },
            {
                id : 11,
                Dorsal: 13,
                Nombre: "Jan Oblak",
                Posicion : "Portero"
            },
    ]

    const CrudApp = () => {

        const [db, setDb] = useState(initalDb)

        const [dataToEdit, setDataToEdit] = useState(null)

        const createData = (data) => {
            data.id = Date.now()
            console.log('creando', data)
            setDb([...db,data])
            alert("Jugador creado con exito")
        }

        const updateData = (data) => {
            let upData = db.map(element => (element.id === data.id ? data : element))
            setDb(upData)
            alert("Jugador actualizado con exito")
        }

        const deleteData = (id) => {

            console.log("initalDb", db)
            console.log("id", id)

            let indexReg = db.findIndex(element => element.id == id);
            
            if(indexReg>=0){
                let newDta = db.filter(element => element.id !== id)

                console.log(newDta)
                
                setDb(newDta);

                alert("Eliminación correcta")
            }else{
                alert("No se pudo encontrar este registro")
            }
        }

        return (
            <div>
                <h2>CRUD ESTATICO REACT</h2>
                <CrudForm initalDb={db} createData={createData} updateData={updateData} setDataToEdit={setDataToEdit} dataToEdit={dataToEdit} />
                <CrudTable data={db} setDataToEdit={setDataToEdit} deleteData={deleteData} />
            </div>
        )
    }

    export default CrudApp
