const SongLyric = ({song}) => {

    let StylesBlockquote = {
        whiteSpace : "pre-wrap"
    }
    
    return (
        <div>
            <h3>Letra de la Cancion</h3>
            <blockquote style={StylesBlockquote}>
                {song.lyrics}
            </blockquote>
        </div>
    )
}

export default SongLyric
