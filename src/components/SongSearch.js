import React, { useState, useEffect } from 'react';
import { helpHttp } from '../helpers/helpHttp';
import Loader from './Loader';

import SongDetail from "./SongDetail"
import SongForm from "./SongForm"

const SongSearch = () => {

    const [search, setSearch] = useState(null)
    const [lyric, setLiryc] = useState(null)
    const [bio, setBio] = useState(null)
    const [loading, setLoading] = useState(false)

    useEffect(() => {

        if(search === null)return;

        const fetchData = async () => {
            const {nameArtist, nameSong } = search; 

            let artistUrl = `https://theaudiodb.com/api/v1/json/1/search.php?s=${nameArtist}`
            let songUrl = `https://api.lyrics.ovh/v1/${nameArtist}/${nameSong}`

            console.log(artistUrl,songUrl)

            setLoading(true)

            const [artistResp,songResp] = await Promise.all(
                    [
                        helpHttp().get(artistUrl),
                        helpHttp().get(songUrl)
                    ]
                )

            console.log(artistResp,songResp)
            setBio(artistResp)
            setLiryc(songResp)
            setLoading(false)

        }

        fetchData()
    }, [search])

    const handleSearch = (data)=>{
        console.log(data)

        if(data.nameArtist===""){
            setSearch(null)
            return null
        }

        setSearch(data)
    }

    return (
        <div>
            <h2>Buscador de canciones</h2>
            {loading && <Loader/>}
            <SongForm handleSearch={handleSearch} />
            {search && !loading && <SongDetail bio={bio} song={lyric} search={search} />}
        </div>
    )
}

export default SongSearch
