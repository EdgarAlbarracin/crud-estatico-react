import Message from "./Message"
import SongArtist from "./SongArtist"
import SongLyric from "./SongLyric"

const SongDetail = ({bio, song, search}) => {

    let Styles2 = {
        padding:"1rem",
        margin:"1rem",
    }

    console.log(search, bio, song)

    if(!bio || !song) return null;

    return (
        <div style={Styles2}>
            {song.error || song.err || song.name === "AbortError"? <Message  Msg={`Error no se encontro la canción: '${search.nameSong}'`} bgColor="#dc3545" /> : <SongLyric song={song} />}
            {!bio.artists? <Message  Msg={`Error no se encontro el artista: '${search.nameArtist}'`} bgColor="#dc3545" /> : <SongArtist bio={bio} />}

        </div>
    )
}

export default SongDetail
