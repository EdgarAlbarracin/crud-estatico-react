import CrudTableRow from "./CrudTableRow"
import Table from 'react-bootstrap/Table'
const CrudTable = ({data, setDataToEdit, deleteData}) => {

    // console.log("dataTable",data)
    return (
        <div>
            <h3 className="title-separated">Tabla de datos</h3>
            <Table className="table-crud" striped bordered hover variant="dark">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Dorsal</th>
                        <th>Posicion</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>                    
                        {data.length>0?(data.map(el => <CrudTableRow setDataToEdit={setDataToEdit}  deleteData={deleteData} key={el.Dorsal} jugadores={el} />)):<tr><td colSpan="4">Sin Datos</td></tr>}
                </tbody>
            </Table>
        </div>
    )
}

export default CrudTable
