import React, { useState, useEffect } from 'react';
import { InputGroup, FormControl, Button } from 'react-bootstrap';
import { AiFillCustomerService,AiFillSmile,AiOutlineSearch } from 'react-icons/ai';
import { GiBroom } from 'react-icons/gi';

const initialForm = {
    nameArtist: "",
    nameSong: "",
}

const SongForm = ({handleSearch,handleClean}) => {

    const [form, setForm] = useState(initialForm)

    const handleChange = (e) => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = (e) => {
        e.preventDefault()

        if(!form.nameArtist || !form.nameSong){
            alert("Datos incompletos")
            return
        }      
        handleSearch(form)
        // handleReset()
    }
    
    const handleReset = async (e) => {
        setForm(initialForm)
        handleSearch(initialForm)
    }

    return (
        <div>
            <form onSubmit={handleSubmit}>
            <InputGroup className="mb-3">
                <InputGroup.Text id="basic-addon1"><AiFillSmile/></InputGroup.Text>
                <FormControl type="text" name="nameArtist" placeholder="Nombre del interprete" onChange={handleChange} value={form.nameArtist} />
            </InputGroup>

            <InputGroup className="mb-3">
                <InputGroup.Text id="basic-addon1"><AiFillCustomerService/></InputGroup.Text>
                <FormControl type="text" name="nameSong" placeholder="Nombre de la canción" onChange={handleChange} value={form.nameSong}  />
            </InputGroup>
            
            <div className="container-buttons">
                <Button variant="outline-light" type="reset" onClick={handleReset}><GiBroom/> Limpiar</Button>
                <Button variant="success" type="submit" value="Buscar"><AiOutlineSearch/> Buscar</Button>
            </div>
            </form>            
        </div>
    )
}

export default SongForm
