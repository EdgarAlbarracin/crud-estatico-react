const SongArtist = ({bio}) => {

    let StylesImg = {
        width: "45vw",
        margin: "3vw",
        borderRadius: "50%",
    }

    console.log(bio.artists[0].strBiographyES)
    return (
        <div>
            <h2>Artista: {bio.artists[0].strArtist}</h2>

            <img style={StylesImg} src={bio.artists[0].strArtistThumb} alt={bio.artists[0].strArtist} />
            <p>
                {bio.artists[0].strBiographyES}
            </p>
        </div>
    )
}

export default SongArtist
