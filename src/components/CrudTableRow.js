import { Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { AiOutlineEdit,AiOutlineDelete } from 'react-icons/ai';

const CrudTableRow = ({jugadores, setDataToEdit, deleteData}) => {
    // console.log(jugadores.Posicion)
    return (
        <tr>
            <td>{jugadores.Nombre}</td>
            <td>{jugadores.Dorsal}</td>
            <td>{jugadores.Posicion}</td>
            <td className="td-actions">            
                <Button variant="outline-warning" onClick={()=>setDataToEdit(jugadores)}><AiOutlineEdit/> Editar</Button>
                <Button variant="outline-danger" onClick={()=>deleteData(jugadores.id)}><AiOutlineDelete/> Eliminar</Button>
            </td>
        </tr>
    )
}

export default CrudTableRow
